Rails.application.routes.draw do


  # ログインしてない時のパス
  root  "static_pages#home"

  get '/users/show',   to: "users#show"
  get "/users/edit",   to: "users#edit"
  get "/signup",       to: "users#new"
  post "/signup",      to: "users#create"
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]

  resources :posts, only: [:index, :new, :create, :show, :destroy] do
    resources :photos, only: [:create]

    resources :likes, only: [:create, :destroy], shallow: true

    resources :comments, only: [:index, :create, :destroy], shallow: true

  end
  resources :relationships,       only: [:create, :destroy]
end
