class Post < ApplicationRecord
  belongs_to :user
  has_many :photos, dependent: :destroy

  has_many :likes, -> { order(created_at: :desc) }, dependent: :destroy
  has_many :comments, -> { order(:created_at => :desc) }, dependent: :destroy

  def liked_by(user)
    # user_idとpost_idが一致するlikeを検索する
    Like.find_by(user_id: user.id, post_id: id)
  end

  def is_belongs_to?(user)
    Post.find_by(user_id: user.id, id: id)
  end

  def self.search(search) #ここでのself.はMicropost.を意味する
    if search
      where(['caption LIKE ?', "%#{search}%"]) #検索とcontentの部分一致を表示。Micropost.は省略。
    else
      all #全て表示。Micropost.は省略。
    end
  end
end
